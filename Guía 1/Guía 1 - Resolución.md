# Estructuras de Datos: Guía 1

## 1. El Entorno Interactivo de Python
### Ejercicio 1 
#### Realizar los siguientes cálculos:
#### 2<sup>1024</sup>
```python
>>> 2**1024
179769313486231590772930519078902473361797697894230657273430081157732675805500963132708477322407536021120113879871393357658789768814416622492847430639474124377767893424865485276302219601246094119453082952085005768838150682342462881473913110540827237163350510684586298239947245938479716304835356329624224137216
```
#### 1/3 + 1/5
```python
>>> (1/3)+(1/5)
0.5333333333333333
```
#### Calcular las raíces del siguiente polinomio X<sup>2</sup> − 8X + 15
```python
>>> import math
>>> x1 = (8 + math.sqrt((-8)**2 - 4*1*15))/(2*1)
>>> x2 = (8 - math.sqrt((-8)**2 - 4*1*15))/(2*1)
>>> print("x1 = " + str(x1) + ", x2 = " + str(x2))
x1 = 5.0, x2 = 3.0
```
### Ejercicio 2
#### Insertar un fin de línea en `saludo` para que lo imprima en 3 líneas
```python
>>> a = 3
>>> saludo = 'Hola Mundo'
>>> saludo = saludo + '\n'
>>> print(saludo*a)
Hola Mundo
Hola Mundo
Hola Mundo
```
#### Reemplazar en `texto` "Java" por "Pyton"
```python
>>> texto = 'En Estructuras de Datos usamos Java como lenguaje de programacion'
>>> texto = texto.replace("Java", "Python")
>>> print(texto)
En Estructuras de Datos usamos Python como lenguaje de programacion
```
 #### ¿Qué caracter devuelve `texto[-2]`?
```python 
>>> texto[-2]
'o'
```
#### ¿Y `texto[-2:]`?
```python
>>> texto[-2:]
'on'
```
#### ¿Qué pasa si ejecutamos `texto[-2] = '-'` ? ¿Porqué?
```python
>>> texto[-2] = '-'
Traceback (most recent call last):  
File "<stdin>", line 1, in <module>
TypeError: 'str' object does not support item assignment
```

  Los string son inmutables

#### ¿En que posición comienza la palabra 'programacion'?
```python
>>> texto.find("programacion")
55
```
#### Extraer la última palabra de `texto` y almacenarla en una nueva variable.
```python
>>> palabras = texto.split(" ")
>>> ultima = palabras[-1]
>>> print(ultima)
programacion
```

#### Extraer la palabra "Datos" de `texto` y almacenarla en una nueva variable.
```python
>>> palabras = texto.split(" ")
>>> indice = palabras.index("Datos")
>>> palabra_datos = palabras[indice]
>>> print(palabra_datos)
Datos
```

#### ¿Que resultado produce `texto.title()`?
```python
>>> texto.title()
'En Estructuras De Datos Usamos Python Como Lenguaje De Programacion'
```

#### Ejecutar el comando `dir(texto)`
```python
>>> dir(texto)
['__add__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
```

#### ¿De que tipo es la variable `texto`?
```python
>>> type(texto)
<class 'str'>
```

#### Investigar la ayuda para la función join que soporta `texto`
```python
>>> help(texto.join)
Help on built-in function join:

join(...) method of builtins.str instance
    S.join(iterable) -> str

    Return a string which is the concatenation of the strings in the iterable. The separator between elements is S.
```

### Ejercicio 4
#### Dado el archivo *cocomp*.*py*
```python
#!/usr/bin/env python3
def codigo_compuesto():
    for x in 'Hola':
        print(x)
    print('listo')
```

#### Ejecutar en *IDLE* la función `codigo_compuesto()` ¿Qué imprime?
```python
>>> codigo_compuesto()
H
o
l
a
listo
```

#### Editarlo y programar una función que reciba un valor en grados Fahrenheit y devuelva el equivalente en grados Celsius.
```python
#!/usr/bin/env python3
def fahrenheit_a_celsius(grados_f):
    grados_c = (grados_f - 32) * (5/9)
    return grados_c
```

#### En *IDLE* utilizar la función anterior e imprimir una tabla de conversión desde 0 ºF hasta 120 ºF de 10 en 10.
```python
>>> for grado in range(0, 121, 10):
        print(str(grado) + " ºF = " + str(fahrenheit_a_celsius(grado)) + " ºC")
0 ºF = -17.77777777777778 ºC
10 ºF = -12.222222222222223 ºC
20 ºF = -6.666666666666667 ºC
30 ºF = -1.1111111111111112 ºC
40 ºF = 4.444444444444445 ºC
50 ºF = 10.0 ºC
60 ºF = 15.555555555555557 ºC
70 ºF = 21.11111111111111 ºC
80 ºF = 26.666666666666668 ºC
90 ºF = 32.22222222222222 ºC
100 ºF = 37.77777777777778 ºC
110 ºF = 43.333333333333336 ºC
120 ºF = 48.88888888888889 ºC
```

#### ¿Qué significa la primera línea del archivo *cocomp*.*py*?
**[Shebang](https://es.wikipedia.org/wiki/Shebang)** es, en la jerga de [Unix](https://es.wikipedia.org/wiki/Unix "Unix"), el nombre que recibe el par de caracteres **#!** que se encuentran al inicio de los [programas ejecutables](https://es.wikipedia.org/wiki/Programa_ejecutable "Programa ejecutable") interpretados. Este método estándar permite que el usuario pueda ejecutar un programa interpretado como si ejecutara un programa binario.
Cuando no se conoce la ruta absoluta del mismo, es posible utilizar el programa auxiliar _env_, que generalmente suele estar en /usr/bin. Este es un método muy usado para _scripts_ donde el intérprete no tiene la misma ruta en todos los sistemas (por ejemplo, el _Shebang_ usado generalmente para [Python](https://es.wikipedia.org/wiki/Python "Python") es "#!/usr/bin/env python", que es una llamada a _env_). Para Python 3, se usa "#!/usr/bin/env python3".

---

## 2. Listas, diccionarios y tuplas
### Ejercicio 5
#### Dada una lista de números escribir una función que:

#### Devuelva una lista con todos los números que sean primos
```python
def es_primo(numero):
"""Función auxiliar para calcular un número primo"""
    if numero <= 1:
        return False
    for i in range(2, numero):
        if numero % i == 0:
            return False
    return True
    
def primos(lista):
    lista_de_primos = []
    for numero in lista:
        if es_primo(numero):
            lista_de_primos.append(numero)
    return lista_de_primos
```

O `primos` con una lista por comprensión:
```python
def primos(lista):
    return [numero for numero in lista if es_primo(numero)]
```

Podemos optimizar la función `es_primo`.
Recorriendo hasta la raíz cuadrada del número:
```python
import math
def es_primo(numero):
    if numero <= 1:
        return False
    max_div = math.ceil(math.sqrt(numero))
    for i in range(2, max_div):
        if numero % i == 0:
            return False
    return True
```

Recorriendo solo divisores impares:
```python
import math
def es_primo(numero):
    if numero <= 1:
        return False
    if  numero == 2:
        return True
    if numero > 2 and numero % 2 == 0:
        return False
    max_div = math.ceil(math.sqrt(numero))
    for i in range(3, max_div, 2):
        if numero % i == 0:
            return False
    return True
```

#### Devuelva la suma y el promedio de todos los números de la lista
```python
def suma_y_promedio(lista):
    if len(lista) == 0:
        return (0, 0)
    else:
        suma = 0
        for numero in lista:
            suma += numero
        return (suma, suma/len(lista))
```

O con los módulos *math* y *statistics*:
```python
import math
import statistics
def suma_y_promedio(lista):
    return (math.fsum(lista), statistics.mean(lista))
```

#### Devuelva una lista con el factorial de cada uno de los números de la lista original
```python
import math
def lista_factoriales(lista):
    factoriales = []
    for numero in lista:
        factoriales.append(math.factorial(numero))
    return factoriales
```

O con una lista por comprensión:
```python
import math
def lista_factoriales(lista):
    return [math.factorial(numero) for numero in lista]
```

#### Reciba la lista y un numero *k* y devuelva tres listas, una con los números menores a *k*, otra con los iguales a *k* y la tercera con los mayores que *k*. (¿Cómo se hace para devolver tres listas?)
```python
def menores_iguales_mayores(lista, k):
    menores = []
    iguales = []
    mayores = []
    for numero in lista:
        if numero < k:
            menores.append(numero)
        elif numero > k:
            mayores.append(numero)
        else:
            iguales.append(numero)
    return (menores, iguales, mayores)
```

O con listas por comprensión (Solo para mostrar listas por comprensión con *if*, nótese que no es la mejor forma de resolverlo ya que estamos iterando tres veces la lista):
```python
def menores_iguales_mayores(lista, k):
    menores = [numero for numero in lista if numero < k]
    iguales = [numero for numero in lista if numero == k]
    mayores = [numero for numero in lista if numero > k]
    return (menores, iguales, mayores)
```
### Ejercicio 6
#### Dada la siguiente representación de una matriz (usando listas):

```math 
\left[ {\begin{array}{ccc} 
1 & 2 & 3 \\
4 & 5 & 6 \\
7 & 8 & 9
\end{array} } \right]
```
```math
[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
```

#### Programar una función que:
#### Reciba dos matrices cuadradas de tamaño arbitrario y devuelva la suma
```python
def suma_de_matrices(matriz_1, matriz_2):
    matriz_suma = matriz_1
    tam = len(matriz_2)
    for fila in range(tam):
        for columna in range(tam):
            matriz_suma[fila][columna] += matriz_2[fila][columna]
    return matriz_suma
```

#### Reciba una matriz y devuelva una lista con los elementos de la diagonal
```python
def diagonal(matriz):
    lista_diagonal = []
    tam = len(matriz)
    for fila in range(tam):
        for columna in range(tam):
            if fila == columna:
                lista_diagonal.append(matriz[fila][columna])
    return lista_diagonal
```

O con una lista por comprensión:
```python
def diagonal(matriz):
    lista_diagonal = [matriz[i][i] for i in range(len(matriz))]
    return lista_diagonal
```

#### Reciba un entero *n* y Devuelva una matriz identidad de *n* x *n*
```python
def identidad(n):
    matriz_identidad = [[0 for i in range(n)] for j in range(n)]
    for fila in range(n):
        for columna in range(n):
            if fila == columna:
                matriz_identidad[fila][columna] = 1
    return matriz_identidad
```

O con una lista por comprensión:
```python
def identidad(n):
    matriz_identidad = [[1 if i==j else 0 for j in range(n)] for i in range(n)]
    return matriz_identidad
```

#### Reciba una matriz de *n* x *n* y devuelva cada columna de la matriz en una lista
```python
def columnas(matriz):
    tam = len(matriz)
    tupla_columnas = ()
    for col in range(tam):
        columna = [matriz[fila][col] for fila in range(tam)]
        tupla_columnas += (columna, )
    return tupla_columnas
```

#### Reciba una matriz y devuelva la matriz traspuesta
```python
def transpuesta(matriz):
    m = len(matriz)
    n = len(matriz[0])
    matriz_transpuesta = [[matriz[i][j] for i in range(m)] for j in range(n)]
    return matriz_transpuesta
```

### Ejercicio 7
#### Dada una cadena de caracteres escribir una función que:
####  Devuelva un diccionario con la frecuencia de aparición de cada caracter
```python
def frecuencias(cadena):
    dic_frecuencias = {}
    for caracter in cadena:
        if caracter not in dic_frecuencias:
            dic_frecuencias[caracter] = 0
        dic_frecuencias[caracter] += 1
    return dic_frecuencias
```
       
#### Reciba además de la cadena de caracteres un caracter *k* y devuelva la frecuencia de *k* en la cadena (si *k* no aparece debe devolver 0)
```python
def frecuencia_k(cadena, k):
    return cadena.count(k)
```

#### Devuelva un diccionario cuyas claves sean las vocales y los valores una lista de palabras que empiecen con la vocal dada(suponer que dentro de la cadena las palabras están separadas por coma y no hay espacios intermedios)
```python
def frecuencias_vocales(cadena):
    dic_frecuencias = {'a':[], 'e':[], 'i':[], 'o':[], 'u':[]}
    lista_palabras = cadena.split(',')
    for palabra in lista_palabras:
        inicial = palabra[0]
        if inicial in dic_frecuencias:
            dic_frecuencias[inicial].append(palabra)
    return dic_frecuencias
```