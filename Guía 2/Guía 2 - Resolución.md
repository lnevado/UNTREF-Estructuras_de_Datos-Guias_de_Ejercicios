
# Estructuras de Datos: Guía 2

## 1. Archivos
### Ejercicio 1
####  Escribir las siguientes funciones y guardarlas en el archivo _caesar_._py_:

```python
def cifrar(frase, clave):
    abc = 'abcdefghijklmnopqrstuvwxyz'
    cifrado = ''
    for caracter in frase:
        if caracter.islower() and caracter in abc:
            idx_letra = abc.find(caracter)
            cifrado += abc[(idx_letra + clave) % 26]
        else:
            cifrado += caracter
    return cifrado
    
def descifrar(frase_cifrada, clave):
    abc = 'abcdefghijklmnopqrstuvwxyz'
    frase = ''
    for caracter in frase_cifrada:
        if caracter.islower() and caracter in abc:
            idx_letra = abc.find(caracter)
            frase += abc[(idx_letra - clave) % 26]
        else:
            frase += caracter
    return frase
```

Usando `ord` y `char`:

```python
def cifrar(frase, clave):
    cifrado = ''
    for caracter in frase:
        idx = ord(caracter)
        if ord('a') <= idx <= ord('z'):
            car_pos = ord(caracter) - ord('a')
            nuevo_idx = (ord('a') + (car_pos + clave) % 26)
            cifrado += chr(nuevo_idx)
        else:
            cifrado += caracter
    return cifrado
    
def descifrar(frase_cifrada, clave):
    frase = ''
    for caracter in frase_cifrada:
        idx = ord(caracter)
        if ord('a') <= idx <= ord('z'):
            car_pos = ord(caracter) - ord('a')
            nuevo_idx = (ord('a') + (car_pos - clave) % 26)
            frase += chr(nuevo_idx)
        else:
            frase += caracter
    return frase
```

### Ejercicio 3 
#### Programar las siguientes funciones y agregarlas a _caesar_._py_:
```python
def cifrar_archivo(entrada, salida, clave):
    with open(entrada, "r") as texto_entrada, open(salida, "w") as texto_salida:
        for linea in texto_entrada.readlines():
            texto_salida.write(cifrar(linea, clave))
    
def descifrar_archivo(entrada, salida, clave):
    with open(entrada, "r") as texto_entrada, open(salida, "w") as texto_salida:
        for linea in texto_entrada.readlines():
            texto_salida.write(descifrar(linea, clave))
```

### Ejercicio 4 
#### Dada la clase `Cifrador` (clase generica que realiza cifrado por sustitución), escribir la clase `CifradorCaesar` que hereda de `Cifrador` (*cifrador*.*py*)
```python
import cifrador
class CifradorCaesar(cifrador.Cifrador):
    def __init__(self, clave_caesar):
        super().__init__()
        self.clave = clave_caesar
        self.alfabeto_cifrado = self.alfabeto_cifrado[self.clave:] + self.alfabeto_cifrado[:self.clave]
```

### Ejercicio 5 
#### Escribir el test unitario para CifradorCeasar, con los mismos casos de prueba que en el caso anterior
```python
import caesar2
import unittest
import random

class TestCaesar(unittest.TestCase):
    def setUp(self):
        self.frases=['Rosita Wachenchauzer', 'estructura de datos', 'Martín Albarracín']
        self.cifradas3=['Rrvlwd Wdfkhqfkdxchu', 'hvwuxfwxud gh gdwrv', 'Mduwíq Aoeduudfíq']
        
    def test_clave_cero(self):
        """Asegurarse que con clave 0 nos da la misma frase
        """
        cifrador = caesar2.CifradorCaesar(0)
        for f in self.frases:
            self.assertEqual(cifrador.cifrar(f), f)
       
        
    def test_cifrar(self):
        """Asegurarse que cifra bien con frases (sin normalizar) conocidas
        """        
        clave=3
        cifrador = caesar2.CifradorCaesar(clave)
        for i in range (len(self.frases)):
            self.assertEqual(cifrador.cifrar(self.frases[i]), self.cifradas3[i])
        
    def test_cifrar_descifrar(self):
        """Asegurarse que si ciframos y desciframos con la misma clave
        se obtiene de nuevo la frase original
        """
        clave=random.randint(0, 26)
        cifrador = caesar2.CifradorCaesar(clave)
        for f in self.frases:
            self.assertEqual(cifrador.descifrar(cifrador.cifrar(f)), f)
        

if __name__ == '__main__':
    unittest.main()
```